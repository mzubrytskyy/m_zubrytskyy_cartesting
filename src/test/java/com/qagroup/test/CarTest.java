package com.qagroup.test;

import com.qagroup.Car;

import org.testng.Assert;
import org.testng.annotations.*;

public class CarTest {

	@Test
	public void carNumberTest () {
		int expectedCarNumber = 3;
		
		int initCarNumber = Car.numberOfCreatedCars();
		
		Car car1 = new Car("BMW", "Black");
		Car car2 = new Car("Volvo", "Siver");
		Car car3 = new Car("VW", "Green");
		
		int actualCarNumber = Car.numberOfCreatedCars() - initCarNumber;
		
		Assert.assertEquals(actualCarNumber, expectedCarNumber);
	}
	
	@Test
	public void carCheck () {
				
		String expectedModel = "BMW";
		String expectedColor = "Black";
		Car car = new Car("BMW", "Black");
		
		String initialModel = car.getModel();
		String initialColor = car.getColor();
		
		Assert.assertEquals(initialModel, expectedModel);
		Assert.assertEquals(initialColor, expectedColor);
		
			}
	
	@Test
	public void changeColorCheck () {
		
		String Model = "Seat";
		String Color = "Blue";
		Car car = new Car(Model, Color);
		String changedColor ="Silver";
		String initialChangedColor = "Silver";
		String expectedColor = car.changeColor(changedColor);
		
		Assert.assertEquals(initialChangedColor, expectedColor);
		
	}
	
//	@AfterMethod
//	public void nulifyCounter () {
//	Car.nulifyCounter();
//	}
	
	}